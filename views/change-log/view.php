<?php

use app\utils\CommonView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\ChangeLog */

$this->title = $model::tableLabel();
$this->params['breadcrumbs'][] = ['label' => $model->tableLabel(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$ref = $model->getReferencedModel();
?>
<div class="change-log-view">

    <div class="page-header">
        <h1>
            <?= $this->title ?>
        </h1>
    </div>

    <table class="table table-striped table-bordered">
        <tr>
            <td>
                <?= $model->getAttributeLabel('id') ?>
            </td>
            <td class="text-info" onclick="$('.time').toggleClass('hidden')">
                <?= $model->getAttribute('id') ?>
            </td>
        </tr>
        <tr class="time">
            <td>
                <?= $model->getAttributeLabel('create_time') ?>
            </td>
            <td class="text-info">
                <?= $model->getAttribute('create_time') ?>
            </td>
        </tr>
        <tr>
            <td>
                <?= $model->getAttributeLabel('user_id') ?>
            </td>
            <td class="text-info">
                <a class="link" href="<?= Url::toRoute(['user/view', 'id' => $model->user_id]) ?>">
                    <?= CommonView::badge($model->user_id, 16) ?>
                    <?= $model->user_full_name ?>
                </a>
            </td>
        </tr>
        <tr>
            <td>
                <?= $model->getAttributeLabel('table') ?>
            </td>
            <td class="text-info">
                <?= $ref ? $ref::tableLabel() : '' ?>
                {<?= $model->table ?>}
            </td>
        </tr>
        <tr>
            <td>
                <?= $model->getAttributeLabel('reference_id') ?>
            </td>
            <td class="text-info">
                <?= $model->reference_id ?>
				<?=	yii\helpers\Html::a('历史记录', ['history', 'table' => $model->table, 'reference_id' => $model->reference_id]) ?>
            </td>
        </tr>
        <tr>
            <td>
                <?= $model->getAttributeLabel('action') ?>
            </td>
            <td class="text-info">
                <?= $model->getActionText() ?>
            </td>
        </tr>
        <tr>
            <td>
                <?= $model->getAttributeLabel('old') ?>
            </td>
            <td>
                <table class="table table-bordered table-condensed">
                    <?php
					$old = \yii\helpers\Json::decode($model->old);
					if($ref){
						foreach ($old as $k => $v){
                    ?>
                    <tr>
                        <td title="<?= $k ?>" class="text-info">
                            <?= $ref->getAttributeLabel($k) ?>
                        </td>
                        <td>
                            <?= $ref->textValue($k) ?>
                        </td>
                    </tr>
                    <?php
						}
					}else{
						foreach ($old as $k => $v){
                    ?>
                    <tr>
                        <td class="text-info">
                            <?= $k ?>
                        </td>
                        <td>
                            <?= $v ?>
                        </td>
                    </tr>
                    <?php
						}
					}
					?>
                </table>
            </td>
        </tr>
    </table>

</div>
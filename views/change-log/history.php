<?php

use app\utils\CommonView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $history array [[changeLog, model]] */
/* @var $modelNow app\utils\CommonActiveRecord */

if(!$modelNow) return;
$this->title = $modelNow::tableLabel() . '#' . $modelNow->id . ' 更新历史';
$this->params['breadcrumbs'][] = ['label' => $modelNow::tableLabel(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$attributeNames = array_keys($modelNow->getAttributes(null, ['id', 'create_time', 'user_id', 'user_full_name', 'auth_key']));
?>
<div class="change-log-view">

    <div class="page-header">
        <h1>
			<?= yii\helpers\Html::a('&lt;', ['index'], ['class' => 'btn btn-default pull-left']) ?>
            <?= $this->title ?>
        </h1>
    </div>

    <table class="table table-striped table-bordered">
		<tr>
			<td>
				<?= $history[0]['changeLog']->getAttributeLabel('create_time') ?>
			</td>
			<td>
				<?= $history[0]['changeLog']->getAttributeLabel('action') ?>
			</td>
			<td>
				<?= $history[0]['changeLog']->getAttributeLabel('user_id') ?>
			</td>
			<td>
				<?= $history[0]['changeLog']->getAttributeLabel('user_full_name') ?>
			</td>
			<?php foreach($attributeNames as $attr){ ?>
			<td>
				<?= $modelNow->getAttributeLabel($attr) ?>
			</td>
			<?php } ?>
		</tr>
		<?php foreach($history as $i => $hist){ ?>
		<tr>
			<td>
				<?= $history[$i]['changeLog']->create_time ?>
			</td>
			<td>
				<?= $history[$i]['changeLog']->getActionText() ?>
			</td>
			<td>
				<?= $history[$i]['changeLog']->user_id ?>
			</td>
			<td>
				<?= $history[$i]['changeLog']->user_full_name ?>
			</td>
			<?php foreach($attributeNames as $attr){ ?>
			<td>
				<?= $history[$i]['model']->textValue($attr) ?>
			</td>
			<?php } ?>
		</tr>
		<?php } ?>
    </table>

</div>
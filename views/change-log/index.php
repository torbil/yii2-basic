<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$model = new app\models\ChangeLog();
$this->title = $model::tableLabel();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="change-log-index">

    <div class="page-header">
        <h1><?= $this->title ?></h1>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'create_time',
			'user_id',
			'user_full_name',
            [
                'attribute' => 'table',
                'label'=> $model->getAttributeLabel('table'),
                'value' => function ($model, $key, $index, $column){
					$ref = $model->getReferencedModel();
                    return $ref ? $ref::tableLabel() : $model->table;
                }
            ],
			'reference_id',
            [
                'attribute' => 'action',
                'label'=> $model->getAttributeLabel('action'),
                'value' => function ($model, $key, $index, $column){
                    return $model->getActionText();
                }
            ],

            'old:ntext',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
        ],
    ]); ?>


</div>
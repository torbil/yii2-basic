<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\domain\User */
/* @var $form yii\widgets\ActiveForm */
$step = 1;

$user = new User();
$this->title = '新增' . User::tableLabel();
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1>
        <?= $this->title ?>
    </h1>

    <div class="user-form">

        <?php $form = ActiveForm::begin(['id' => 'user-form']); ?>

        <table class="table table-bordered">
            <tr>
                <td class="col-xs-4 col-md-2">
                    <?= $step++ ?>. <?= $model->getAttributeLabel('name') ?>
                </td>
                <td>
                    <?= $form->field($model, 'name')->label(false) ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= $step++ ?>. <?= $model->getAttributeLabel('password') ?>
                </td>
                <td>
                    <?= $form->field($model, 'password')->textInput(['maxlength'])->label(false) ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= $step++ ?>. <?= $model->getAttributeLabel('full_name') ?>
                </td>
                <td>
                    <?= $form->field($model, 'full_name')->label(false) ?>
                </td>
            </tr>
            <tr>
                <td>s
                    <?= $step++ ?>. <?= $model->getAttributeLabel('role') ?>
                </td>
                <td>
                    <?= $form->field($model, 'role')->dropDownList(User::getRoleItems(\Yii::$app->user->getIdentity()->role))->label(false) ?>
                </td>
            </tr>
        </table>

        <div class="form-group text-center">
            <?= Html::submitButton('保存', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        <?php $this->registerJs($this->render('js-create', ['model' => $model])); ?>
    </div>

</div>

<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
$step = 1;

$this->title = '#' . $model->id . ' 修改' . User::tableLabel();
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-update">

    <h1>
        <?= $this->title ?>
    </h1>

    <div class="user-form">

        <?php $form = ActiveForm::begin(['id' => 'user-update']); ?>

        <table class="table table-bordered">
            <tr>
                <td class="col-xs-4 col-md-2">
                    <?= $step ?>. <?= $model->getAttributeLabel('name') ?>
                </td>
                <td>
            <?php if(!\app\models\User::iAmOperator()){ ?>
                <?= $form->field($model, 'name')->label(false) ?>
            <?php }else{ ?>
                <?= $model->name ?>
            <?php } ?>
                </td>
            </tr>
            <tr>
                <td class="update-toggle">
                    <?= $step ?>. <?= $model->getAttributeLabel('password') ?>
                </td>
                <td>
                    <div class="update-input">
                        <?= $form->field($model, 'password')->textInput(['maxlength', 'placeholder' => '不改密码则不要填写'])->label(false) ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <?= $step ?>. <?= $model->getAttributeLabel('full_name') ?>
                </td>
                <td>
            <?php if(!\app\models\User::iAmOperator()){ ?>
                    <?= $form->field($model, 'full_name')->label(false) ?>
            <?php }else{ ?>
                <?= $model->full_name ?>
            <?php } ?>
                </td>
            </tr>
            <?php if(Yii::$app->user->getIdentity()->role > $model->role){ //only change lower roles ?>
            <tr>
                <td>
                    <?= $step ?>. <?= $model->getAttributeLabel('role') ?>
                </td>
                <td>
                    <?= $form->field($model, 'role')->dropDownList(User::getRoleItems(\Yii::$app->user->getIdentity()->role))->label(false) ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?= $step ?>. <?= $model->getAttributeLabel('state') ?>
                </td>
                <td>
                    <?= $form->field($model, 'state')->dropDownList(User::getStateItems())->label(false) ?>
                </td>
                </tr>
            <?php } ?>
            <tr>
                <td></td>
                <td>
                    <?= Html::submitButton('保存', ['class' => 'btn btn-success']) ?>
                </td>
            </tr>
        </table>

        <?php ActiveForm::end(); ?>
        <?php $this->registerJs($this->render('js-update', ['model' => $model])); ?>
    </div>

</div>

<?php
use yii\helpers\Html;
/* @var $this \yii\web\View */
/* @var $model \app\models\domain\Tally */
if (false){
?>
<script><?php }?>
var $passwordInput = $('#<?= Html::getInputId($model, 'password') ?>'),
    $passwordValidator = CustomValidator($passwordInput);
var $form = $('#user-update');
var modalId = 'errorModal';
$('body').prepend(modalHtml(modalId, '错误信息'));
var $modal = $('#' + modalId);
$passwordInput.change(validateForm);
function validateForm() {
    var isOk = true;
    $passwordValidator.clearError();
    if($passwordInput.val().length > 0 && $passwordInput.val().length < 6){
        isOk = false;
        $passwordValidator.markError();
        $passwordValidator.showMessage('密码长度要6个以上');
    }
    return isOk;
}

$form.on('beforeSubmit', function(event){
    if(!validateForm()){
        return false;
    }
    $.post($form.prop('action'), $form.serialize(), function (response) {
        if (!response.ok) {
            $modal.modalMessage(response.message);
            $modal.modal();
        } else {
            location.replace(response.redirect);
        }
    }, 'json');
    return false;
});

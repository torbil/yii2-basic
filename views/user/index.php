<?php

use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$user = new User();
$this->title = $user::tableLabel();
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    ['class' => 'yii\grid\SerialColumn'],

    'id',
    'name',
	'full_name',
    [
        'attribute' => 'role',
        'label'=> $user->getAttributeLabel('role'),
        'value' => function ($model, $key, $index, $column){
            /** @var $model User */
            return $model->getRoleText();
        }
    ],
    [
        'attribute' => 'state',
        'label'=> $user->getAttributeLabel('state'),
        'value' => function ($model, $key, $index, $column){
            /** @var $model User */
            return $model->getStateText();
        }
    ],
    'create_time',

    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{view} &nbsp; &nbsp; {update}'
    ],
];
?>
<div class="user-index">

    <div class="page-header">
        <h1><?= $this->title ?></h1>
    </div>

    <?php if(Yii::$app->user->getIdentity()->hasCreateAccess()){ ?>
        <p>
            <?= Html::a('新增' . User::tableLabel(), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php } ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
    ]); ?>


</div>
<?php

use app\models\User;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = User::tableLabel();
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="user-view">

    <h1>
        <?= $this->title ?>
    </h1>

    <p>
        <?= Html::a('修改', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('新增' . User::tableLabel(), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-striped table-bordered detail-view'],
        'attributes' => [
            [
                'attribute' => 'id',
                'captionOptions' => ['class' => 'col-xs-4 col-md-2'],
            ],
            [
                'attribute' => 'role',
                'label' => $model->getAttributeLabel('role'),
                'value' => function ($model){
                    return $model->getRoleText();
                }
            ],
            'name',
			'full_name',
            [
                'attribute' => 'state',
                'label' => $model->getAttributeLabel('state'),
                'value' => function ($model){
                    return $model->getStateText();
                }
            ],
        ]
    ]) ?>
</div>

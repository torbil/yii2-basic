<?php

/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = Yii::$app->name;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>
            <?= Yii::$app->name ?>
		</h1>

        <div class="lead alert alert-info">
			设置
        </div>

        <div>
            <a class="btn btn-lg btn-default text-center" href="<?php echo Url::to(['user/index']); ?>">
        		<?php echo \app\models\User::tableLabel() ?>
            </a>
            <a class="btn btn-lg btn-default text-center" href="<?php echo Url::to(['change-log/index']); ?>">
        		<?php echo \app\models\ChangeLog::tableLabel() ?>
            </a>
        </div>

        <div><a class="btn btn-info" href="<?= Url::toRoute(['/gii']) ?>">Gii Panel &raquo;</a></div>
    </div>

</div>

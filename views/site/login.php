<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use app\models\User;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = '用户登录';
$this->params['breadcrumbs'][] = $this->title;
$user = new User();
?>
<style>
	#login-form table * {
		font-size: 16px
	}
</style>
<div class="site-login table-bordered" style="width: 400px; margin: 50px auto">
    <h3 class="text-center bg-primary" style="margin: 0; padding: 15px 0">
        <?= Yii::$app->name ?>
    </h3>
    <h2 class="text-center"><?= $this->title ?></h2>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'fieldConfig' => [
            'template' => "{input}\n{error}",
        ],
    ]); ?>

    <table class="table">
        <tr>
            <td>
                <?= $form->field($model, 'username')->dropDownList(User::getLoginItems(), ['autofocus' => true]) ?>
            </td>
        </tr>
        <tr>
            <td>
                <?= $form->field($model, 'password')->passwordInput(['placeholder' => $user->getAttributeLabel('password')]) ?>
            </td>
        </tr>
        <tr>
            <td class="text-center">
                <?= Html::submitButton('登录', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </td>
        </tr>
    </table>

    <?php ActiveForm::end(); ?>
</div>

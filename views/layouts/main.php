<?php
/* @var $this \yii\web\View */

/* @var $content string */

use app\models\User;
use app\utils\Breadcrumbs;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

$menuItems = Breadcrumbs::getMenuItems($this);
$this->beginContent('@app/views/layouts/basic.php'); ?>
<style>
    .navcontainer .navbar-header {
        float: none;
        padding-bottom: 10px;
    }

    .navcontainer li.active a {
        background-color: #dc3545;
        color: #fff;
    }
</style>
<div class="container-fluid">
    <div class="col-sm-3 col-xs-12">
        <div class="navcontainer">
            <?php
            NavBar::begin([
                'brandLabel' => Yii::$app->name,
                'brandUrl' => Yii::$app->homeUrl,
                'innerContainerOptions' => ['class' => 'text-center'],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'nav-tabs nav-stacked text-left'],
                'items' => $menuItems,
            ]);
            NavBar::end();
            ?>


        </div>
    </div>
	<div class="col-sm-9 col-xs-12">
		<?= $content ?>
	</div>
</div>
<?php $this->endContent(); ?>

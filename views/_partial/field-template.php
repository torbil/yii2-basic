            <div class="input-group">
                {label}
                {hint}<?php if($prefixAddon != null){ ?>
                <span class="input-group-addon text-info">
                    <?= $prefixAddon ?>
                </span>
<?php }?>
                {input}<?php if($addonContent != null){ ?>
                <span class="input-group-addon"><?= $addonContent ?></span>
<?php }?>

            </div>
            {error}

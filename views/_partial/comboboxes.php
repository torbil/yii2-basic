<?php if(false){ ?><script><?php } ?>
$(function(){
    var source ={
            datatype: "json",
            datafields: [
                { name: 'id', type: 'int'},
                { name: 'name', type: 'string'}
            ],
            url: "<?= $comboUrl ?>",
            async: false
            },
        dataAdapter = new $.jqx.dataAdapter(source);
    function goCombo(inputId, comboId){
        var $input = $("#" + inputId);
        $input.after('<div id="' + comboId + '"></div>');
        var $combo = $("#" + comboId);
        $combo.on('bindingComplete', function (event) {
            if(!($input.val() > 0))return;
            var item = $combo.jqxComboBox('getItemByValue', $input.val());
            $combo.jqxComboBox('selectItem', item);
        }).on('select', function(event){
            $input.val(event.args.item.value).trigger('change');
        }).jqxComboBox({
            source: dataAdapter,
            autoComplete: true,
            width: $combo.parents('.combo-container').width() - 20,
            height: 30,
            displayMember: 'name',
            valueMember: 'id'
        });
    }
<?php
foreach($models as $i => $model){
    $attr = "[$i]$attribute";
    $inputId = yii\helpers\Html::getInputId($model, $attr);
    $comboId = $inputId . '_combo';
?>
    goCombo('<?= $inputId ?>', '<?= $comboId ?>');
<?php
}
?>
});
<?php
/**
 * usage:
 * 1. in form (change 2 things: (1)attribute name 'picture' (2)js view file name 'js-form'):

<?= $form->field($model, 'picture', ['template' => CommonView::templateAddon($this, '...')])
->textInput(['maxlength' => true, 'readonly' => true])->label(false) ?>
...
<img id="img-<?= basename(get_class($model)) ?>-picture" class="img" style="max-width: 100%" src="<?= $model->picture ?>">
...
<?php $this->registerJs($this->render('js-form', ['model' => $model])) ?>

 * 2. in js-form (change 1 things: attribute name 'picture'):

    <?= $this->render('../_partial/js-image-chooser') ?>
    ImageChooser.bindForModelAttribute('<?= basename(get_class($model)) ?>', 'picture');

 */
if(0){ ?><script><?php } ?>
    var ImageChooser = (function () {
        var modalId = 'imgModal';
        if($('#' + modalId).length == 0){
            $('body').append(modalHtml(modalId, '选择图片 | رەسم تاللاڭ'));
        }
        var $modal = $('#' + modalId);

        var $targetInput, $targetImg;
        function showImgDialogFor($input, $img){
            $targetInput = $input;
            $targetImg = $img;
            $modal.modal();
        }
        $modal.delegate('img', 'click', function () {
            var src = $(this).attr('src');
            $modal.modal('hide');
            $targetInput.val(src);
            $targetImg.attr('src', src);
        });

<?php
        echo '/*';
        $imgPath = 'images';
        $imgDir = dir(Yii::getAlias('@app/web/' . $imgPath));
        $imgs = [];
        $maxCount = 100; //no too much images allowed on the dialog
        while ($file = $imgDir->read()){
            if(is_file($imgDir->path . '/' . $file) && strlen($file) > 4 && in_array(strtolower(substr($file, -4)), ['.jpg', '.png'])){
                $imgs[] = $file;
                if(count($imgs) > $maxCount) break;
            }
        }
        echo '*/';
        ?>
        var imgPath = '<?= $imgPath ?>', imgs = <?= \yii\helpers\Json::encode($imgs) ?>;
        updateImgModal(imgPath, imgs);
        function updateImgModal(imgPath, imgs) {
            var imgHtml, imgsHtml = '<div class="container-fluid">';
            for(var i = 0; i < imgs.length; i++){
                imgHtml = '<div class="col-sm-2" style="height: 200px"><img class="img img-thumbnail" src="' + imgPath + '/' + imgs[i] + '" style="max-height: 100%">' + imgs[i] + '</div>';
                imgsHtml += imgHtml;
            }
            imgsHtml += '</div>';
            $modal.modalMessage(imgsHtml);
        }

        /**
         * only for input-group with add-on
         * @param $input
         * @param $img
         */
        function bind($input, $img) {
            $input.parents('.input-group').find('.input-group-addon').on('click', function () {
                showImgDialogFor($input, $img);
            });
        }

        /**
         * img id should be in the form of
         * **"img-{modeName}-{attribute}"**
         * e.g. <img id="img-Building-picture">
         * @param modelName
         * @param attribute
         */
        function bindForModelAttribute(modelName, attribute) {
            var $inputPicture = $('input[name="' + modelName + '[' + attribute + ']"]'), $imgPicture = $('#img-' + modelName + '-' + attribute);
            bind($inputPicture, $imgPicture);
        }


        return {
            imgPath : imgPath,
            imgs : imgs,
            updateImgModal : updateImgModal,
            $modal : $modal,
            showImgDialogFor : showImgDialogFor,
            bind : bind,
            bindForModelAttribute : bindForModelAttribute
        };
    })();
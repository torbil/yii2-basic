<?php

use app\models\Address;
use app\models\Building;
use app\models\Floor;
use app\models\Project;
use app\models\Room;
use app\models\User;
use app\utils\Bi;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $data array */
$address = new Address($data);
?>
<table class="table table-condensed">
<?php
function renderRoom($address){
    ob_start();
?>
    <tr>
        <td class="<?= User::getRtl() ?>">
            <?= $address->room->name ?>
        </td>
    </tr>
<?php
    return ob_get_clean();
}
function renderFloor($address){
ob_start();
?>
    <tr>
        <td class="<?= User::getRtl() ?>">
            <?= $address->floor->getNameBi() ?>
        </td>
    </tr>
<?php
    return ob_get_clean();
}
function renderBuilding($address){
ob_start();
?>
    <tr>
        <td class="<?= User::getRtl() ?>">
            <?= $address->building->getNameBi() ?>
        </td>
    </tr>
<?php
    return ob_get_clean();
}
?>
    <tr>
        <td class="<?= User::getRtl() ?>">
            <?= $address->project->getNameBi() ?>
        </td>
    </tr>
<?php
if ($address->from == Address::FROM_ROOM){
    echo renderBuilding($address);
    echo renderFloor($address);
    echo renderRoom($address);
}
if ($address->from == Address::FROM_FLOOR){
    echo renderFloor($address);
}
if ($address->from == Address::FROM_BUILDING){
    echo renderBuilding($address);
}
?>
</table>

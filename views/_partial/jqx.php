<?php
use app\utils\Common;
use yii\helpers\Url;
$jqxPathPrefix = Yii::getAlias('@web') . '/jqwidgets/';
$jqxJsFiles = [ 
   'jqxcore.js', 
   'jqxbuttons.js', 
   'jqxscrollbar.js', 
   'jqxlistbox.js', 
   'jqxdata.js', 
   'jqxcombobox.js', 
   ];
foreach($jqxJsFiles as $jsFile){
   $this->registerJsFile($jqxPathPrefix . $jsFile, ['depends' => [yii\web\JqueryAsset::className()]]);
}
function renderFieldAndRegisterComboJs($view, $form, $model, $attribute, $comboUrl = null){
    echo $form->field($model, $attribute)->hiddenInput(['maxlength' => true])->label(false);
    echo \yii\helpers\Html::error($model, $attribute, ['class' => 'help-block']);
    registerComboboxJs($view, $model, $attribute, $comboUrl);
}
function registerComboboxJs($view, $model, $attribute, $comboUrl = null){
    $comboUrl = $comboUrl ? $comboUrl : Url::to([Common::refAttrToRouteId($attribute) . '/combo']);
    $comboViewParams = [
        'model' => $model,
        'attribute' => $attribute,
        'comboUrl' => $comboUrl
    ];
    $view->registerJs($view->render('../_partial/combobox', $comboViewParams));
}
function comboboxes($view, $models, $attribute){
    $comboViewParams = [
        'models' => $models,
        'attribute' => $attribute,
        'comboUrl' => Url::to([Common::refAttrToRouteId($attribute) . '/combo'])
        ];
    $view->registerJs($view->render('../_partial/comboboxes', $comboViewParams));
}
?>
<link rel="stylesheet" href="<?= $jqxPathPrefix ?>styles/jqx.base.css" type="text/css" />

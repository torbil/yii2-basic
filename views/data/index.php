<?php

/* @var $this \yii\web\View */

use app\models\User;
use yii\helpers\Html;

?>

<h1>
	<div class="btn-group">
		<?= Html::a('<', ['/'], ['class' => 'btn btn-default en']) ?>
		<div class="btn btn-primary">
			导入
		</div>
		<?= Html::a('备份', ['backup'], ['class' => 'btn btn-default']) ?>
		<?= Html::a('还原', ['restore'], ['class' => 'btn btn-default']) ?>
	</div>
	数据导入与备份
</h1>
<?= $form = Html::beginForm(); ?>
<table class="table table-condensed" id="inputs">
		<td colspan="2" style="width:330px">
			<div class="input-group">
				<span class="input-group-addon">
					项目名称
				</span>
				<input name="projectName" id="projectName" value="" type="text" class="form-control">
			</div>
		</td>
		<td style="width:150px">
			<div class="input-group">
				<span class="input-group-addon">
					楼栋栏
				</span>
				<input name="buildingColumn" id="buildingColumn" value="1" type="text" class="form-control">
			</div>
		</td>
		<td>
			<div class="input-group">
				<span class="input-group-addon">
					物业栏
				</span>
				<input name="roomFeeColumn" id="roomFeeColumn" value="2" type="text" class="form-control">
			</div>
		</td>
		<td>
			<div class="input-group">
				<span class="input-group-addon">
					跳过首行
				</span>
				<span class="input-group-addon">
				<input name="skipFirstLine" id="skipFirstLine" type="checkbox" checked="checked">
				</span>
			</div>
		</td>
		<td>
			<div class="btn btn-default" id="previewBtn">预览</div>
		</td>
	</tr>
</table>

<table width="100%">
	<tr>
		<td class="col-sm-8" valign="top">
			<?php if($processedLines > 0 || count($badLines) > 0){ ?>
				<h3>
					成功处理 <?= $processedLines ?> 行。
				</h3>
				<?php if(($errorCount = count($badLines)) > 0){ ?>
				<h3 class="text-danger">
					发现<?= $errorCount ?>个问题。 具体问题及行号:
				</h3>
				<pre><?= implode("\r\n", $badLines) ?></pre>
				<?php
				}
			}else{
				?>
			<textarea name="xls" id="xls" class="form-control" style="min-height: 300px; height: 100%;"></textarea>
				<?php
			} ?>
		</td>
		<td valign="top">
			项目名称: <span id="projectName0"></span><br>
			楼栋: <span id="buildingColumn0"></span><br>
			物业: <span id="roomFeeColumn0"></span><br>
			<hr>
			<?= Html::submitButton('开始导入', ['class' => 'btn btn-lg btn-primary', 'onclick' => '$(this).addClass("disabled");']) ?>
		</td>
		<td valign="top" id="originalColumns" style="width:150px">
			
		</td>
	</tr>
</table>
<?= Html::endForm(); ?>
<?php $this->registerJs($this->render('index-js')) ?>
<?php

/* @var $this \yii\web\View */

use yii\helpers\Html;
?>

<h1>
	<div class="btn-group">
		<?= Html::a('<', ['/'], ['class' => 'btn btn-default en']) ?>
		<?= Html::a('导入', ['index'], ['class' => 'btn btn-default']) ?>
		<?= Html::a('备份', ['backup'], ['class' => 'btn btn-default']) ?>
		<div class="btn btn-primary">
			还原
		</div>
	</div>
	数据导入与备份
</h1>

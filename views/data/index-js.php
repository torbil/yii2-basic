<?php if(0){ ?><script><?php } use yii\helpers\Html;?>
$('#inputs input').change(preview);
$('#previewBtn').click(preview);
$('#xls').change(preview);
function preview(){
	var columns = $('#xls').val().split("\n")[0].split("\t");
	$('#inputs input').each(function(){
		previewColumn(this, columns);
	});
}
function previewColumn(input, columns){
	var text = '', index = $(input).numVal();
	if(index > 0){
		if(columns.length < index) text = '<span class="text-danger">超出总栏数</span>';
		else text = columns[index - 1];
	}else if(input.id == 'projectName'){
		text = input.value;	
	}
	$('#' + input.id + '0').html(text);
	var columnsTable = '<table width="100%">';
	for(var i in columns){
		columnsTable += '<tr><td>' + (i/1 + 1) + '</td><td>' + columns[i] + '</td></tr>';
	}
	columnsTable += '</table>';
	$('#originalColumns').html(columnsTable)
}
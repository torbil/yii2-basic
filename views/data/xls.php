<?php

/* @var $this \yii\web\View */

use app\models\Project;

/* @var $badLines string[] */
/* @var $projectsNotFound string[] */
/* @var $projects Project[] */
?>

<h1>
    سىفىر تەپسىلاتى
</h1>

<div class="rtl">
    <div class="col-md-6">
        <h2>
            تېپىلمىغان تۈرلەر
        </h2>
        <?php
        foreach ($projectsNotFound as $projectName) {
            echo $projectName . '<br>';
        }
        ?>
        <h2>
            ئىناۋەتسىز قۇرلار
        </h2>
        <p>
            <?php
            foreach ($badLines as $badLine) {
                echo $badLine . '<br>';
            }
            ?>
        </p>
    </div>
    <div class="col-md-6">
        <h2>
            تېپىلغان تۈرلەر
        </h2>
        <table class="table table-bordered table-condensed">
            <?php
            foreach ($projects as $project) {
                ?>
            <tr>
                <td>
                    <?= $project->id ?>
                </td>
                <td>
                    <?= $project->name ?>
                </td>
                <td>
                    <?= $project->name_zh ?>
                </td>
            </tr>
                <?php
            }
            ?>
        </table>
    </div>
</div>

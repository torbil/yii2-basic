function modalHtml(id, title) {
    title = title ? title : '-';
    return '<div id="' + id + '" class="modal fade" tabindex="-1" role="dialog">\n' +
            '    <div class="modal-dialog" style="width: 85%" role="document">\n' +
            '        <div class="modal-content">\n' +
            '            <div class="modal-header">\n' +
            '                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n' +
            '                <h4 class="modal-title text-center">' + title + '</h4>\n' +
            '            </div>\n' +
            '            <div class="modal-body">\n' +
            '            </div>\n' +
            '            <div class="modal-footer">\n' +
            '                <button type="button" class="btn btn-danger" data-dismiss="modal">关闭</button>\n' +
            '                <!--<button type="button" class="btn btn-primary">Save changes</button>-->\n' +
            '            </div>\n' +
            '        </div><!-- /.modal-content -->\n' +
            '    </div><!-- /.modal-dialog -->\n' +
            '</div><!-- /.modal -->\n';
}
function prepareModal(id, title){
    var modalSelector = '#' + id,
        $modal = $(modalSelector);
    if($modal.length == 0){
        $('body').append(modalHtml(id, title));
        $modal = $(modalSelector);
    }else
        $modal.modalTitle(title);
    return $modal;
}
function fixNum(n){
  return isNaN(n) || (n + '').trim() == '' ? 0 : parseFloat((n / 1).toFixed(2));
}
$(function($){
    $.fn.extend({
        numVal : function(){
            return fixNum(this.val());
        },
        /**
         * @returns {OffscreenRenderingContext | CanvasRenderingContext2D | WebGLRenderingContext}
         */
        getContext2d : function () {
            return this.get(0).getContext('2d');
        },
        canvasClear : function () {
            this.getContext2d().closePath();
            this.getContext2d().clearRect(0, 0, this.width(), this.height());
            this.getContext2d().beginPath();
            return this;
        },
        canvasReset : function () {
            this.attr({width: this.attr('width')});
            return this;
        },
        canvasBeginDot : function (x, y) {
            this.getContext2d().beginPath();
            this.getContext2d().moveTo(x, y);
            return this;
        },
        canvasStrokeLine : function (x, y) {
            this.getContext2d().lineTo(x, y);
            this.getContext2d().stroke();
            return this;
        },
        canvasPoly : function (dots) {
            if(dots.length < 2) return;
            this.canvasBeginDot(dots[0], dots[1]);
            for (var i = 2; i < dots.length; i += 2) {
                this.getContext2d().lineTo(dots[i], dots[i + 1]);
            }
            this.getContext2d().lineTo(dots[0], dots[1]);
            this.getContext2d().stroke();
            this.getContext2d().fill();
            this.getContext2d().closePath();
            return this;
        },
        canvasStroke : function () {
            this.getContext2d().stroke();
            return this;
        },
        canvasFill : function () {
            this.getContext2d().fill();
            return this;
        },
        canvasStrokeFillSettings : function (lineWidth, lineStyle, fillStyle) {
            this.getContext2d().lineWidth = lineWidth;
            this.getContext2d().strokeStyle = lineStyle;
            this.getContext2d().fillStyle = fillStyle;
            return this;
        },
        makeMapOverlay : function(){
            var $imageMaps = this.parent().find('div.image-map');
            $imageMaps.each(function () {
                var $imageMap = $(this),
                    cssAbs = {position: 'absolute', left: 0, top: 0},
                    $img = $imageMap.css('position', 'relative').find('img').css(cssAbs).css('opacity', 0).before('<canvas class="canvas" height="30"></canvas>'),
                    $map = $imageMap.find('map[name="' + $img.attr('usemap').substr(1) + '"]'),
                    $canvas = $imageMap.find('canvas.canvas').css(cssAbs).css({"background-image": 'url(' + $img.attr('src') + ')', 'background-repeat' : 'no-repeat'})
                ;
                $('<img>').on('load', function(){
                    var width = this.width,
                        height = this.height,
                        size = {width: width, height: height};
                    $imageMap.width(width);
                    $imageMap.height(height);
                    $canvas.attr(size);
                    $map.find('area[shape="poly"]').each(function () {
                        var $area = $(this), lineStyle = '#f003', fillStyle = $area.data('color') || '#fcc3';
                        $canvas.canvasStrokeFillSettings(1, lineStyle, fillStyle);
                        $canvas.canvasPoly($area.attr('coords').split(','));
                    });
                }).attr('src', $img.attr('src'));
            });
        },
        modalTitle : function (title) {
            this.find('.modal-title').html(title);
            return this;
        },
        modalMessage : function (messageHtml) {
            this.find('.modal-body').html(messageHtml);
            return this;
        },
        modalShow : function (messageHtml) {
            this.modalMessage(messageHtml).modal();
            return this;
        },
        inputAddon : function () {
            return this.parents('.input-group').find('.input-group-addon');
        }
    });
});
function GraphMarkerFactory($graphInput, $graphCanvas){
    var $graphAddons = $graphInput.inputAddon(),
        dots = [],
        oldDots = [],
        isMarkingPoly = false,
        isRect = false,
        rectDots = [],
        oldRectDots = [],
        lineWidth = 5,
        lineStyle = '#f00',
        fillStyle = '#c009',
        $graphAddonPrefix = $graphAddons.length == 2 ? $graphAddons.first() : null,
        $graphAddonSuffix = $graphAddons.last();

    function drawPoly() {
        setStyle();
        $graphCanvas.canvasPoly(dots);
    }

    $graphAddonSuffix.on('click', function(event){
        stopRect();
        if(isMarkingPoly){
            //finish marking
            //at least 3 points to define an area
            if(dots.length < 6){
                dots = oldDots;
            }
            $graphInput.val(dots.join(','));
            //redraw a full polygon
            $graphCanvas.canvasClear();
            drawPoly();
            stopPoly();
        }else{
            oldDots = dots; //back up
            setStyle();
            //start marking
            $graphAddonSuffix.text('...');
            $graphCanvas.canvasClear();
            //clear
            dots = [];
            isMarkingPoly = true;
        }
    });

    /**
     * dots to input
     * redraw (clear) poly from dots
     */
    function dotsToPoly() {
        $graphInput.val(dots.join(','));
        //redraw a full polygon
        $graphCanvas.canvasClear();
        drawPoly();
    }

    var minWidth = 20, minHeight = 20;
    function rectToDots() {
        if(rectDots.length < 4){
            rectDots = [];
            return;
        }
        var x1 = rectDots[0],
            y1 = rectDots[1],
            x2 = rectDots[2],
            y2 = rectDots[3];
        if(Math.abs(x2 - x1) < minWidth || Math.abs(y2 -  y1) < minHeight){
            rectDots = [];
            return;
        }
        dots = [];
        //x1,y1 same
        dots.push(x1);
        dots.push(y1);
        //x2,y1
        dots.push(x2);
        dots.push(y1);
        //x2,y2
        dots.push(x2);
        dots.push(y2);
        //x1,y2
        dots.push(x1);
        dots.push(y2);
    }

    function stopPoly() {
        isMarkingPoly = false;
        $graphAddonSuffix.text('+');
    }
    function stopRect() {
        isRect = false;
        rectDots = [];
        $graphAddonPrefix.text('[]');
    }

    $graphAddonPrefix.on('click', function(event){
        stopPoly();
        if(isRect){
            //finish marking
            //at least 2 points to define a rectangle
            if(rectDots.length < 4){
                rectDots = oldRectDots;
            }
            rectToDots();
            dotsToPoly();
            stopRect();
        }else{
            oldRectDots = rectDots; //back up
            //start marking
            $graphAddonPrefix.text('...');
            $graphCanvas.canvasClear();
            //clear
            rectDots = [];
            isRect = true;
        }
    });

    function handlePolyClick(event) {
        if(!isMarkingPoly) {
            return;
        }
        dots.push(event.offsetX);
        dots.push(event.offsetY);
        if(dots.length == 2){
            $graphCanvas.canvasBeginDot(dots[0], dots[1]);
        }else{
            $graphCanvas.canvasStrokeLine(dots[dots.length - 2], dots[dots.length - 1]);
        }
    }

    function handleRectBegin(event) {
        if(!isRect){
            return;
        }
        rectDots = [];
        rectDots.push(event.offsetX);
        rectDots.push(event.offsetY);
    }

    function handleRectEnd(event) {
        if(!isRect){
            return;
        }
        rectDots.push(event.offsetX);
        rectDots.push(event.offsetY);
        setStyle();
        $graphCanvas.canvasClear();
        $graphCanvas.getContext2d().rect(rectDots[0], rectDots[1], rectDots[2] - rectDots[0], rectDots[3] - rectDots[1]);
        $graphCanvas.getContext2d().closePath();
        $graphCanvas.canvasStroke().canvasFill();
    }

    $graphCanvas.on('click', handlePolyClick).on('mousedown', handleRectBegin).on('mouseup', handleRectEnd);

    function setStyle() {
        $graphCanvas.canvasStrokeFillSettings(lineWidth, lineStyle, fillStyle);
    }

    function updateBg(src) {
        $('<img>').on('load', function(){
            var size = {width: this.width, height: this.height};
// console.log(size);
            $graphCanvas.attr(size);
            //resizing will clear out the canvas, so re-draw
            if(dots.length > 6){
                drawPoly();
            }
        }).attr('src', src);
        $graphCanvas.css('background-image', 'url("' + src + '")');
    }

    function drawFromGraphInput(){
        dots = $graphInput.val().split(',');
        drawPoly();
    }
    return {
        $input : $graphInput,
        $canvas : $graphCanvas,
        dots : dots,
        updateBg : updateBg,
        drawFromGraphInput : drawFromGraphInput
    }
}
function CustomValidator($input) {
    var $formGroup = $input.parents('.form-group').first(),
        helpBlockClass = '.help-block',
        $helpBlock = $formGroup.find(helpBlockClass),
        successClass = 'has-success',
        errorClass = 'has-error';
    if($helpBlock.length == 0){
        $formGroup.append('<div class="help-block"></div>');
        $helpBlock = $formGroup.find(helpBlockClass);
    }
    function markError(){
        $formGroup.removeClass(successClass);
        $formGroup.addClass(errorClass);
    }
    function clearError(){
        $formGroup.removeClass(errorClass);
        $helpBlock.html('');
    }
    function markSuccess(){
        clearError();
        $formGroup.addClass(successClass);
    }
    function showMessage(msg){
        $helpBlock.html(msg);
    }
    return {
        markError : markError,
        clearError : clearError,
        markSuccess : markSuccess,
        showMessage : showMessage
    }
}
function eventHandlerLastToFirst(element, eventName) {
    var eventList = $._data(element, 'events');
    eventList[eventName].unshift(eventList[eventName].pop());
}
// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
// 例子： 
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
//from datejs
Date.isLeapYear = function (year) { 
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0)); 
};

Date.getDaysInMonth = function (year, month) {
    return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

Date.prototype.isLeapYear = function () { 
    return Date.isLeapYear(this.getFullYear()); 
};

Date.prototype.getDaysInMonth = function () { 
    return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
};
var DATE_FORMAT = 'yyyy-MM-dd';
Date.prototype.addMonths = function (value) {
    var n = this.getDate();
    this.setDate(1);
    this.setMonth(this.getMonth() + value);
    this.setDate(Math.min(n, this.getDaysInMonth()));
    return this;
};
<?php


namespace app\utils;

use app\models\User;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class CommonActiveRecord extends ActiveRecord
{
	public static function tableLabel() {
        return '数据表名称';
    }
	//override
	public function beforeValidate() {
		if (!parent::beforeValidate()) {
			return false;
		}
		if ((!$this->hasAttribute('user_id')) || $this->user_id > 0) {
			return true;
		}
		if(!\Yii::$app->user->isGuest){
			$user = \Yii::$app->user->getIdentity();
			$this->setAttributes([
				'user_id' => $user->id,
				'user_full_name' => $user->full_name,
			], false);
		}
		return true;
	}
	
	const STATE_NORMAL = 1,
          STATE_DELETED = 0,
          STATE_LOCKED = 2;
	/**
     * 
     * used for self::<b>stateItems(~)</b>
     * @return array
     */
    public static function getAllStateKeys() {
        return [
			self::STATE_NORMAL,
			self::STATE_DELETED,
			self::STATE_LOCKED
		];
	}
    /**
     * State keys available for current ActiveRecord.
     * Used for self::<b>stateItems(~)</b> in forms.
     * Should be <b>overriden</b> in concrete ActiveRecord model.
     * @see <i>self::getAllStateKeys()</i>
     * @return array
     */
    public static function getStateKeys() {
        return self::getAllStateKeys();
    }
	/**
     * Should be <b>overriden</b> in concrete ActiveRecord model.
	 * @see <i>self::getStateKeys()</i>
	 * @return array for DropDownList
	 */
    public static function getStateItems() {
        return [
			self::STATE_NORMAL => '正常',
			self::STATE_DELETED => '已删除',
			self::STATE_LOCKED => '已锁定',
		];
    }
    /**
     * Label for current record state.
     * used in list (index), detail (view) views.
	 * @see <i>self::getStateItems()</i>
     * @return string|int corresponding label or state value if no label found
     */
    public function getStateText() {
        return ArrayHelper::getValue(self::getStateItems(), $this->state, $this->state);
    }
	public function textValue($attr){
		$textMethod = \yii\helpers\Inflector::camelize('get_' . $attr . '_text');
		return method_exists(static::class, $textMethod)
				? call_user_func(array($this, $textMethod))
				: $this->getAttribute($attr)
				;
	}

    public function setNonEmptyString($attribute, $value){
        if (Common::isEmpty($value)) {
			$value = '-';
		}
		$this->setAttribute($attribute, $value);
    }
    /**
     * Saves $this, but throws Exception if fails.
     * See **BiActiveRecord::saveOrThrow()**
     * @throws Exception
     */
    public function trySave(){
        static::saveOrThrow($this);
    }
    /**
     * saves given ActiveRecord, but throw Exception
     *  with <b>errorSummary</b> as message, if not saved
     * @param ActiveRecord $activeRecord
     * @throws Exception
     */
    public static function saveOrThrow($activeRecord) {
//        if(!$activeRecord->save())
//            throw new Exception(implode("\n", $activeRecord->getErrorSummary (true)));
        $activeRecord->save();
        self::throwIfHasError($activeRecord);
    }
    public static function throwIfHasError($activeRecord) {
        if($activeRecord->hasErrors()) throw new Exception(implode("\n", $activeRecord->getErrorSummary (true)));
    }

    public static function getAllTableNames(){
        $tableNames = [];
        $classNames = self::getAllTableClassNames();
        foreach ($classNames as $class){
            $tableNames[] = $class::tableName();
        }
        return $tableNames;
    }
    public static function getAllTableClassUrls(){
        $classLinks = [
            \app\models\ChangeLog::class => 'change-log/view',
            User::class => 'user/view',
        ];
        return $classLinks;
    }
    public static function getAllTableClassNames(){
        $classNames = array_keys(self::getAllTableClassUrls());
        return $classNames;
    }
    public static function getClassFor($tableName){
        foreach (self::getAllTableClassNames() as $class){
            if($tableName == $class::tableName()){
				return $class;
            }
        }
        return null;
    }
    public static function getModelFor($tableName){
        if($class = self::getClassFor($tableName)){
			return new $class;
		}
        return null;
    }

    public static function getViewRouteFor($class, $id, $params){
        $route = [ArrayHelper::getValue(self::getAllTableClassUrls(), $class), 'id' => $id];
        if ($params) {
			$route = ArrayHelper::merge($route, $params);
		}
		return $route;
    }
    public function getViewLink($withHistory = false){
        return Html::a(static::tableLabel(), self::getViewRouteFor(static::class, $this->id, $withHistory ? ['history' => 1] : []));
    }
	
}
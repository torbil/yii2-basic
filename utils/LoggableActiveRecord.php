<?php


namespace app\utils;

use app\models\Area;
use app\models\ChangeLog;
use app\models\Management;
use app\models\Room;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LoggableActiveRecord extends CommonActiveRecord
{
	/**
	 * override
	 */
	public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $tableName = static::tableName();
        //no recording for ChangeLog, else infinite loop?
        //no recording for User.init, else no ChangeLog.user_id
        $isChangeLogLoop = $tableName == ChangeLog::tableName();
        $isUserInit = $tableName == User::tableName() && $this->id == User::INIT_ID && $insert;
        if($isChangeLogLoop || $isUserInit){return;}

        if($insert){
//            ChangeLog::inserted($tableName, $this->id);
        }else{
            if(!empty($changedAttributes)){
                ChangeLog::updated($tableName, $this->id, $changedAttributes);
            }
        }
    }
	/**
	 * override
	 */
    public function afterDelete()
    {
        parent::afterDelete();
        ChangeLog::deleted(static::tableName(), $this->id, $this->getAttributes());
    }

}
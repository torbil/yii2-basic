<?php

namespace app\utils;

use \yii\helpers\Html;

class CommonView {
    public static function badge($n, $size = 0, $color = 'primary'){
        return '<span class="label label-' . $color . '"' . ($size > 0 ? ' style="font-size:' . $size . 'px"' : '') . '>' . $n . '</span>';
    }

    /**
     * 
     * @param yii\web\View $view
     * @param string $addonContent
     * @param string $prefixAddon null if no
     * @return string ActiveField.template
     */
    public static function templateAddon($view, $addonContent, $prefixAddon = null) {
        return $view->render('/_partial/field-template', ['addonContent' => $addonContent, 'prefixAddon' => $prefixAddon]);
    }

}

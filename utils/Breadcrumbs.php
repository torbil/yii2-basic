<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\utils;

use app\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Description of Breadcrums
 *
 * @author Administrator
 */
class Breadcrumbs extends \yii\widgets\Breadcrumbs {
    public static function getMenuItems(\yii\web\View $view)
    {
        $menuItems = [
            Breadcrumbs::getHomeLinkItem(),
            [
                'label' => \app\models\User::tableLabel(),
                'url' => ['/user'],
            ],
			'<li class="text-success">设置</li>',
			'<li>' . Html::a('数据导入与备份', ['/data']) . '</li>',
        ];
        if(User::iAmManager() || User::iAmAdmin()){
            $menuItems = ArrayHelper::merge($menuItems, [
				[
					'label' => \app\models\ChangeLog::tableLabel(),
					'url' => ['/change-log'],
				],
            ]);
        }
        if(\Yii::$app->user->id == \app\models\User::INIT_ID){
            $menuItems[] =     [
                'label' => 'Gii',
                'url' => ['/gii'],
            ];
        }
		if(!\Yii::$app->user->isGuest){
			$menuItems[] = '<li>'
						. Html::beginForm(['/site/logout'], 'post')
						. Html::submitButton(
							'<span class="badge">' . \Yii::$app->user->identity->full_name . '</span> 退出',
							['class' => 'btn btn-default logout']
						)
						. Html::endForm()
						. '</li>';
		}
        if(!empty($view->params['activeUrl'])){
            foreach ($menuItems as &$item){
                if(is_array($item) && $view->params['activeUrl'] == \yii\helpers\ArrayHelper::getValue($item, 'url')){
                    $item['active'] = true;
                }
            }
        }
        return $menuItems;
    }

    public static function getHomeLabel(){
        return '首页';
    }
    public static function getHomeUrl(){
        return \Yii::$app->homeUrl;
    }
    public function init(){
        parent::init();
        $this->homeLink = self::getHomeLinkItem();
    }

    /**
     * @return array menu item for nav
     */
    public static function getHomeLinkItem() {
        return [
            'label' => self::getHomeLabel(),
            'url' => self::getHomeUrl(),
        ];
    }

}

<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$modelClassName = Inflector::camel2words(StringHelper::basename($generator->modelClass));
$nameAttributeTemplate = '$model->' . $generator->getNameAttribute();
$titleTemplate = $generator->generateString('Update ' . $modelClassName . ': {name}', ['name' => '{nameAttribute}']);
if ($generator->enableI18N) {
    $title = strtr($titleTemplate, ['\'{nameAttribute}\'' => $nameAttributeTemplate]);
} else {
    $title = strtr($titleTemplate, ['{nameAttribute}\'' => '\' . ' . $nameAttributeTemplate]);
}

echo "<?php\n";
?>

use yii\helpers\Html;
use app\utils\Bi;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

<?php
$columnNames = $generator->getColumnNames();
$nameVar = '\'#\' . $model->id . \' \' . ';
if(in_array('name_zh', $columnNames)){
    $nameVar .= '$model->getNameBi()';
}else{
    $nameVar .=  '$model->' . $generator->getNameAttribute();
}
?>
$this->title = $model->strings('update', 'bi', true);
$this->params['breadcrumbs'][] = ['label' => $model->tableLabel(), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => <?= $nameVar ?>, 'url' => ['view', <?= $urlParams ?>]];
$this->params['breadcrumbs'][] = $model->strings('update', 'bi');
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-update">

    <div class="page-header">
        <h1>
            <?= "<?php " ?>Bi::twoSide($this->title, $model->id) ?>
        </h1>
    </div>

    <?= '<?= ' ?>$this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

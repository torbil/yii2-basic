<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use app\utils\Bi;
use app\utils\AR;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use app\utils\Bi;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

<?php
$idVar = '\'#\' . $model->id . \' \' . ';
$nameVar = '';
$columnNames = $generator->getColumnNames();
$hasNameZh = in_array('name_zh', $columnNames);
if($hasNameZh){
    $nameVar .= '$model->getNameBi()';
}else{
    $nameVar .=  '$model->' . $generator->getNameAttribute();
}
?>
$this->title = <?= $nameVar ?>;
$this->params['breadcrumbs'][] = ['label' => $model->tableLabel(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-view">

    <div class="page-header">
        <h1>
            <?= "<?php " ?>Bi::twoSide($this->title, $model->id) ?>
        </h1>
    </div>

    <p>
        <?= "<?= " ?>Html::a($model::strings('update', 'bi'), ['update', <?= $urlParams ?>], ['class' => 'btn btn-primary']) ?>
        <?= "<?= " ?>Html::a($model::strings('delete', 'bi'), ['delete', <?= $urlParams ?>], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => $model::strings('confirmDelete', 'bi'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table class="table table-striped table-bordered">
        <tr>
            <td>
                <?= '<' ?>?= $model->getAttributeLabel('id') ?>
            </td>
            <td colspan="2" class="text-info text-center" onclick="$('.time').toggleClass('hidden')">
                <?= '<' ?>?= $model->getAttribute('id') ?>
            </td>
            <td class="rtl">
                <?= '<' ?>?= $model->getAttributeLabel('id') ?>
            </td>
        </tr>
        <tr class="time hidden">
            <td>
                <?= '<' ?>?= $model->getAttributeLabel('create_time') ?>
            </td>
            <td colspan="2" class="text-info text-center">
                <?= '<' ?>?= $model->getAttribute('create_time') ?>
            </td>
            <td class="rtl">
                <?= '<' ?>?= $model->attributeLabel('create_time', 'ug') ?>
            </td>
        </tr>
        <tr class="time hidden">
            <td>
                <?= '<' ?>?= $model->getAttributeLabel('update_time') ?>
            </td>
            <td colspan="2" class="text-info text-center">
                <?= '<' ?>?= $model->getAttribute('update_time') ?>
            </td>
            <td class="rtl">
                <?= '<' ?>?= $model->attributeLabel('update_time', 'ug') ?>
            </td>
        </tr>
<?php if($hasNameZh){ ?>
        <tr>
            <td>
                <?= '<' ?>?= $model->getAttributeLabel('name_zh') ?>
            </td>
            <td class="text-info">
                <?= '<' ?>?= $model->getAttribute('name_zh') ?>
            </td>
            <td class="rtl text-info">
                <?= '<' ?>?= $model->getAttribute('name') ?>
            </td>
            <td class="rtl">
                <?= '<' ?>?= $model->getAttributeLabel('name') ?>
            </td>
        </tr>
<?php }
        foreach($columnNames as $column){
            if(in_array($column, ['id', 'create_time', 'update_time'])) continue;
            if($hasNameZh && in_array($column, ['name', 'name_zh'])) continue;
        ?>
        <tr>
            <td>
                <?= '<' ?>?= $model->getAttributeLabel('<?= $column ?>') ?>
            </td>
            <td colspan="2" class="text-info text-center">
<?php       if(AR::isRef($column)){ ?>
                <a class="link" href="<?= '<' ?>?= Url::toRoute(['<?= AR::refAttrToRouteId($column) ?>/view', 'id' => $model-><?= $column ?>]) ?>">
                    <?= '<' ?>?= Bi::badge($model-><?= $column ?>, 16) ?>
                    <?= '<' ?>?php if(($parentRef = $model-><?= AR::refAttrToCamel($column) ?>) != null){ ?>
                    <?= '<' ?>?= $parentRef->getNameBi() ?>
                    <?= '<' ?>?php } ?>
                </a>
<?php       }elseif(StringHelper::startsWith($column, 'is_')){ ?>
                <?='<'?>?= Html::activeCheckbox($model, '<?= $column ?>', ['label' => false, 'disabled' => 'disabled']) ?>
<?php       }else{ ?>
                <?= '<' ?>?= $model-><?= $column ?> ?>
<?php       } ?>
            </td>
            <td class="rtl">
                <?= '<' ?>?= $model->attributeLabel('<?= $column ?>', 'ug') ?>
            </td>
        </tr>
<?php
        }
        ?>
    </table>

</div>
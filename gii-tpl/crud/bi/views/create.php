<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

echo "<?php\n";
?>

use yii\helpers\Html;
use app\utils\Bi;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = $model->strings('create', 'bi', true);
$this->params['breadcrumbs'][] = ['label' => $model->tableLabel(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->strings('create', 'bi');
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-create">

    <div class="page-header">
        <h1><?= "<?php " ?>Bi::twoSide($this->title) ?></h1>
    </div>

    <?= "<?= " ?>$this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

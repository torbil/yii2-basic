<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\utils\AR;
use app\utils\Bi;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
<?php
$hasRef = false;
foreach ($generator->getColumnNames() as $attribute) {
    if(app\utils\AR::isRef($attribute)){
        $hasRef = true;
        break;
    }
}
if($hasRef){
?>
include Yii::getAlias('@app') . '/views/_partial/jqx.php';
<?php } ?>
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

    <?= "<?php " ?>$form = ActiveForm::begin(); ?>
<?php
$hasNameZh = in_array('name_zh', $generator->getColumnNames());
if($hasNameZh){
?>

    <div class="row">
        <div class="col-sm-6">
            <?='<'?>?= $form->field($model, 'name_zh')->textInput(['maxlength' => true])->label('名称') ?>
        </div>
        <div class="col-sm-6" dir="rtl">
            <?='<'?>?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('ئىسمى') ?>
        </div>
    </div>
<?php }
foreach ($generator->getColumnNames() as $attribute) {
    if(in_array($attribute, ['create_time', 'update_time'])) continue;
    if($hasNameZh && in_array($attribute, ['name', 'name_zh'])) continue;
    if(!in_array($attribute, $safeAttributes)) continue;
    if(yii\helpers\StringHelper::startsWith($attribute, 'is_')){
        echo "    <div class='row form-group text-center'><?= \$form->field(\$model, '$attribute')->checkbox(['label' => \$model->attributeLabelBi('$attribute')]) ?></div>\n\n";
        continue;
    }
?>

    <div class="row">
        <div class="col-sm-4">
            <?='<'?>?= $model->attributeLabel('<?= $attribute ?>', 'zh') ?>
        </div>
<?php
    if(app\utils\AR::isRef($attribute)){
?>
        <div class="col-sm-4 combo-container">
            <?='<'?>?php renderFieldAndRegisterComboJs($this, $form, $model, '<?= $attribute ?>'); ?>
        </div>
<?php
    }else{
?>
        <div class="col-sm-4">
            <?='<'?>?= <?= $generator->generateActiveField($attribute) ?>->label(false) ?>
        </div>
<?php
    }
?>
        <div class="col-sm-4" dir="rtl">
            <?='<'?>?= $model->attributeLabel('<?= $attribute ?>', 'ug') ?>
        </div>
    </div>

<?php
}
?>
    <div class="row">
        <div class="form-group text-center">
            <?= "<?= " ?>Html::submitButton(Bi::labelSave(), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?= "<?php " ?>ActiveForm::end(); ?>

</div>
<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use app\utils\Common;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use <?= $generator->indexWidgetType === 'grid' ? "yii\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$model = new <?= $generator->modelClass ?>();
$this->title = $model::tableLabel();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">

    <div class="page-header">
        <h1><?= "<?php " ?>$this->title ?></h1>
    </div>

    <p>
        <?= "<?= " ?>Html::a('新增', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?= $generator->enablePjax ? "    <?php Pjax::begin(); ?>\n" : '' ?>
<?php if(!empty($generator->searchModelClass)): ?>
<?= "    <?php " . ($generator->indexWidgetType === 'grid' ? "// " : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
<?php endif; ?>

<?php if ($generator->indexWidgetType === 'grid'): ?>
    <?= "<?= " ?>GridView::widget([
        'dataProvider' => $dataProvider,
        <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
            ['class' => 'yii\grid\SerialColumn'],

<?php
$count = 0;
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        if (++$count < 6) {
            echo "            '" . $name . "',\n";
        } else {
            echo "            //'" . $name . "',\n";
        }
    }
} else {
    foreach ($tableSchema->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        if ((!in_array($column->name, ['create_time', 'update_time'])) && ++$count < 6) {
            if(Common::isRef($column->name)){
?>
            [
                'attribute' => '<?= $column->name ?>',
                'label' => $model->getAttributeLabel('<?= $column->name ?>'),
                'value' => function ($model, $key, $index, $column){
                    return $model-><?= Common::refAttrToCamel($column->name) ?> != null ? '#' . $model-><?= Common::refAttrToCamel($column->name) ?>->id . ' ' .$model-><?= Common::refAttrToCamel($column->name) ?>->name : $model-><?= $column->name ?>;
                }
            ],
<?php
                continue;
            }
            if(in_array($column->name, ['id', 'name', 'full_name'])){
                echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
                continue;
            }
?>
            [
                'attribute' => '<?= $column->name ?>',
                'label'=> $model->getAttributeLabel('<?= $column->name ?>'),
                'value' => function ($model, $key, $index, $column){
                    return $model-><?= $column->name ?>;
                }
            ],

<?php
        } else {
            echo "            //'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        }
    }
}
?>

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php else: ?>
    <?= "<?= " ?>ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
        },
    ]) ?>
<?php endif; ?>

<?= $generator->enablePjax ? "    <?php Pjax::end(); ?>\n" : '' ?>

</div>
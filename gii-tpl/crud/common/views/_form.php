<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
<?php
$hasRef = false;
foreach ($generator->getColumnNames() as $attribute) {
    if(app\utils\Common::isRef($attribute)){
        $hasRef = true;
        break;
    }
}
if($hasRef){
?>
include Yii::getAlias('@app') . '/views/_partial/jqx.php';
<?php } ?>
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

    <?= "<?php " ?>$form = ActiveForm::begin(); ?>
<?php
foreach ($generator->getColumnNames() as $attribute) {
    if(in_array($attribute, ['create_time', 'user_id', 'user_full_name'])) continue;
    if(!in_array($attribute, $safeAttributes)) continue;
    if(yii\helpers\StringHelper::startsWith($attribute, 'is_')){
        echo "    <div class='row form-group text-center'><?= \$form->field(\$model, '$attribute')->checkbox(['label' => \$model->attributeLabelBi('$attribute')]) ?></div>\n\n";
        continue;
    }
?>

    <div class="row">
        <div class="col-sm-4">
            <?='<'?>?= $model->getAttributeLabel('<?= $attribute ?>') ?>
        </div>
<?php
    if(app\utils\Common::isRef($attribute)){
?>
        <div class="col-sm-8 combo-container">
            <?='<'?>?php renderFieldAndRegisterComboJs($this, $form, $model, '<?= $attribute ?>'); ?>
        </div>
<?php
    }else{
?>
        <div class="col-sm-8">
            <?='<'?>?= <?= $generator->generateActiveField($attribute) ?>->label(false) ?>
        </div>
<?php
    }
?>
    </div>

<?php
}
?>
    <div class="row">
        <div class="form-group text-center">
            <?= "<?= " ?>Html::submitButton('保存, ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?= "<?php " ?>ActiveForm::end(); ?>

</div>
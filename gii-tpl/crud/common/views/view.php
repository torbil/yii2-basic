<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use app\utils\Common;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use app\utils\Common;
use app\utils\CommonView;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

<?php
$idVar = '\'#\' . $model->id . \' \' . ';
$columnNames = $generator->getColumnNames();
$nameVar =  '$model->' . $generator->getNameAttribute();
?>
$this->title = <?= $nameVar ?>;
$this->params['breadcrumbs'][] = ['label' => $model::tableLabel(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-view">

    <div class="page-header">
        <h1>
            <?= "<?php " ?>$this->title ?>
        </h1>
    </div>

    <p>
        <?= "<?= " ?>Html::a('修改', ['update', <?= $urlParams ?>], ['class' => 'btn btn-primary']) ?>
        <?= "<?= " ?>Html::a('删除', ['delete', <?= $urlParams ?>], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '确定要删除吗？',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table class="table table-striped table-bordered">
        <tr>
            <td>
                <?= '<' ?>?= $model->getAttributeLabel('id') ?>
            </td>
            <td class="text-info" onclick="$('.time').toggleClass('hidden')">
                <?= '<' ?>?= $model->id ?>
            </td>
        </tr>
        <tr class="time hidden">
            <td>
                <?= '<' ?>?= $model->getAttributeLabel('create_time') ?>
            </td>
            <td class="text-info">
                <?= '<' ?>?= $model->create_time ?>
            </td>
        </tr>
        <tr class="time hidden">
            <td>
                <?= '<' ?>?= $model->getAttributeLabel('user_id') ?>
            </td>
            <td class="text-info">
                <?= '<' ?>?= $model->user_id ?>
            </td>
        </tr>
        <tr class="time hidden">
            <td>
                <?= '<' ?>?= $model->getAttributeLabel('user_full_name') ?>
            </td>
            <td class="text-info">
                <?= '<' ?>?= $model->user_full_name ?>
            </td>
        </tr>
<?php
		foreach($columnNames as $column){
            if(in_array($column, ['id', 'create_time', 'user_id', 'user_full_name'])) continue;
        ?>
        <tr>
            <td>
                <?= '<' ?>?= $model->getAttributeLabel('<?= $column ?>') ?>
            </td>
            <td class="text-info">
<?php       if(Common::isRef($column)){ ?>
                <a class="link" href="<?= '<' ?>?= Url::toRoute(['<?= Common::refAttrToRouteId($column) ?>/view', 'id' => $model-><?= $column ?>]) ?>">
                    <?= '<' ?>?= CommonView::badge($model-><?= $column ?>, 16) ?>
                    <?= '<' ?>?php if(($parentRef = $model-><?= Common::refAttrToCamel($column) ?>) != null){ ?>
                    <?= '<' ?>?= $parentRef->name ?>
                    <?= '<' ?>?php } ?>
                </a>
<?php       }elseif(StringHelper::startsWith($column, 'is_')){ ?>
                <?='<'?>?= Html::activeCheckbox($model, '<?= $column ?>', ['label' => false, 'disabled' => 'disabled']) ?>
<?php       }else{ ?>
                <?= '<' ?>?= $model-><?= $column ?> ?>
<?php       } ?>
            </td>
        </tr>
<?php
        }
        ?>
    </table>

</div>
<?php

namespace app\controllers;

use Yii;
use app\models\User;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(parent::beforeAction($action)){
            $this->getView()->params['activeUrl'] = ['/user'];
            return true;
        }
        return false;
    }

    /**
     * Lists all User models.
     * @return mixed
     * @throws \Throwable
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->getIdentity();
        //if not manager or admin, go to update self info
        if($user->role < User::ROLE_MANAGER){
            return $this->redirect(['view', 'id' => $user->id]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => User::whereForRole(Yii::$app->user->getIdentity()->role),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws HttpException
     * @throws \Throwable
     */
    public function actionCreate()
    {
        if(!Yii::$app->user->getIdentity()->hasCreateAccess()) throw new HttpException(403);
        $model = new User();

        if (Yii::$app->request->isPost) {
            $tx = Yii::$app->db->beginTransaction();
            try {
                $this->handleNewUser($model);
                $result = [
                    'ok' => true,
                    'redirect' => Url::to(['view', 'id' => $model->id]),
                ];
                $tx->commit();
            } catch (Exception $e) {
                $tx->rollBack();
                Yii::error($e->getMessage(), 'UserController.actionCreate');
                $result = [
                    'ok' => false,
                    'message' => $e->getMessage(),
                ];
            }
            return Json::encode($result);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
//        $model->forUpdate();

        if (Yii::$app->request->isPost) {
            $tx = Yii::$app->db->beginTransaction();
            try {
                $this->handleUpdateUser($model);
                $result = [
                    'ok' => true,
                    'redirect' => Url::to(['view', 'id' => $model->id]),
                ];
                $tx->commit();
            } catch (Exception $e) {
                $tx->rollBack();
                Yii::error($e->getMessage(), 'UserController.actionUpdate');
                $result = [
                    'ok' => false,
                    'message' => $e->getMessage(),
                ];
            }
            return Json::encode($result);
        }

        $model->password = null;
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findWhereValid()->andWhere(['id' => $id])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('未找到Model');
    }

    private function handleNewUser(User $model)
    {
        $model->load(Yii::$app->request->post());
        $model->newUser();
        $model->trySave();
    }

    private function handleUpdateUser(User $model)
    {
        $model->load(Yii::$app->request->post());
        $model->updateUser();
        $model->trySave();
    }
}
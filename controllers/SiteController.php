<?php

namespace app\controllers;

use app\models\User;
use app\utils\Breadcrumbs;
use app\utils\Common;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'logout', 't'],
                'rules' => [
                    [
//                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testit' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->getView()->params['activeUrl'] = Breadcrumbs::getHomeUrl();
        return $this->render('index');
    }
	//imitating classic Property Management Systems:
	//unified layout (projects on left side)
	public function actionT(){
		echo 'for testing<br>';
		$this->allModels();
//		$this->camelize();
		$this->copyObject();
	}
	private function allModels(){
		echo "<hr>";
		echo "allModels<br>";
		foreach (\app\utils\LoggableActiveRecord::getAllTableClassNames() as $class){
			$model = new $class;
			echo $class . ': ' . $model->tableName() . '<br>';
		}
	}
	private function camelize(){
		echo "<hr>";
		echo "camelize<br>";
		$strs = explode(",", 'user,change_log');
		foreach ($strs as $str){
			echo $str . ': ' . \yii\helpers\Inflector::camelize($str) . '<br>';
		}
	}
	public function copyObject() {
		//copy object, just copies the reference, NOT cloned
		$user = User::findOne(1);
		echo \yii\helpers\Json::encode($user) . '<br>';
		$user2 = $user;

		$user3 = new User();
		$user3->setAttributes($user->getAttributes());
		
		$user4 = new User();
		$user4->setAttributes(\yii\helpers\Json::decode(\yii\helpers\Json::encode($user)));

		$user2->id = 2;
		$user2->name = 'ikki';
		$user2->full_name = '2 2';

		$user3->name = "vq";

		$user4->name = "tot";

		echo 'user: ' . \yii\helpers\Json::encode($user) . '<br>';
		echo 'user2: ' . \yii\helpers\Json::encode($user2) . '<br>';
		echo 'user3: ' . \yii\helpers\Json::encode($user3) . '<br>';
		echo 'user4: ' . \yii\helpers\Json::encode($user4) . '<br>';
	}

	/**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        User::makeSureInitialized();

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        $this->layout = 'basic';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}

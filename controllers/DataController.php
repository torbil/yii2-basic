<?php

namespace app\controllers;

use yii\web\Controller;
use \yii\helpers\ArrayHelper;

class DataController extends Controller
{
	public $layout = 'basic';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'backup', 'restore'],
                'rules' => [
                    [
//                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
	public function actionIndex(){
		$data = [];
		$data['badLines'] = [];
		$data['processedLines'] = 0;
		if(\Yii::$app->request->post('xls')){
			$this->handleXlsPost($data);
		}
		return $this->render('index', $data);
	}
	private function throwIfEmpty($v, $msg){
		if (empty($v)) {
			throw new \Exception($msg);
		}
	}

	public function actionBackup(){
		//todo to sql
		return $this->render('backup');
	}
	public function actionRestore(){
		//todo from sql
		return $this->render('restore');
	}

	private function handleXlsPost(&$data) {
		//all just dummy code
		$lines = \explode("\r\n", \Yii::$app->request->post('xls'));
		//form values
		$projectName = trim(\Yii::$app->request->post('projectName'));
		$skipFirstLine = \Yii::$app->request->post('skipFirstLine');
		//get column indexes
		$buildingColumn = intval(\Yii::$app->request->post('buildingColumn')) - 1;
		$roomFeeColumn = intval(\Yii::$app->request->post('roomFeeColumn')) - 1;

		if(empty($projectName)){
			$data['badLines'][] = "请填写项目名称";
			return;
		}
		foreach($lines as $i => $line){
			if (($i == 0 && $skipFirstLine) || \app\utils\Common::isEmpty($line)) {
				continue;
			}
			$columns = explode("\t", $line);
			$this->processColumns($data, $i, $columns, $projectName, $buildingColumn, $roomFeeColumn);
		}
	}
	public function processColumns(&$data, $i, $columns, $projectName, $buildingColumn, $roomFeeColumn) {
		try{
			$area = $area = $this->getArea(
					$projectName,
					ArrayHelper::getValue($columns, $buildingColumn)
					);
			$this->throwIfEmpty($area, '楼栋/区域错误');
			$this->throwIfEmpty(
					$this->getRoom($area->id, ArrayHelper::getValue($columns, $roomFeeColumn)),
					"无法添加房屋"
					);
			$data['processedLines'] ++;
		}catch(\Exception $e){
			$data['badLines'][] = ($i + 1) . "\t" . implode("\t", $columns) . "\t" . $e->getMessage();
		}
	}
	private function getArea($projectName, $building)
    {
		//$area = Area::findOne(...);
		//if(!$area)
		//  $area = new Area
		//  $area->... = ...;
		//  if(!$area->save())
		//    return null;
		//return $area;
		return new \yii\base\Object([
			'id' => 1,
			'projectName' => $projectName,
			'building' => $building
			]);
    }
    private function getRoom($areaId, $fee)
    {
		//see getArea()
        return null;
    }

}
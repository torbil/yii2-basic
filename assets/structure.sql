-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 20, 2020 at 08:28 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sandan`
--

-- --------------------------------------------------------

--
-- Table structure for table `change_log`
--

CREATE TABLE `change_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `create_time` datetime NOT NULL DEFAULT current_timestamp() COMMENT '发生时间',
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT '操作员ID',
  `user_full_name` varchar(50) NOT NULL COMMENT '操作员',
  `table` varchar(50) NOT NULL COMMENT '表格',
  `reference_id` bigint(20) UNSIGNED NOT NULL COMMENT '相关记录ID',
  `action` tinyint(3) UNSIGNED NOT NULL COMMENT '操作',
  `old` text DEFAULT NULL COMMENT '备份'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='更新日志';

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `create_time` datetime NOT NULL DEFAULT current_timestamp() COMMENT '记录时间',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1 COMMENT '操作员ID',
  `user_full_name` varchar(50) NOT NULL DEFAULT '系统' COMMENT '操作员',
  `name` char(20) NOT NULL COMMENT '登录名',
  `full_name` varchar(50) NOT NULL COMMENT '全名',
  `password` varchar(120) NOT NULL COMMENT '密码',
  `auth_key` varchar(60) NOT NULL,
  `role` tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '角色',
  `state` tinyint(4) UNSIGNED NOT NULL COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `change_log`
--
ALTER TABLE `change_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_change_log_user1_idx` (`user_id`),
  ADD KEY `table_reference_id` (`table`,`reference_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `change_log`
--
ALTER TABLE `change_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `change_log`
--
ALTER TABLE `change_log`
  ADD CONSTRAINT `change_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

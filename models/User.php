<?php

namespace app\models;

use Yii;
use app\utils\Bi;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $create_time 记录时间
 * @property string $name 登录名
 * @property string $full_name 全名
 * @property string $password 密码
 * @property string $auth_key
 * @property int $role 角色
 * @property int $state 状态
 *
 * @property ChangeLog[] $changeLogs
 */
class User extends \app\utils\LoggableActiveRecord implements \yii\web\IdentityInterface
{
    public static function tableLabel(){
        return '用户';
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['name', 'full_name'], 'required'],
            [['role', 'state'], 'integer'],
			[['state'], 'default', 'value' => self::STATE_NORMAL],
            [['name'], 'string', 'max' => 20],
            [['full_name'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 120],
            [['auth_key'], 'string', 'max' => 60],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => '注册时间',
            'name' => '登录名',
            'full_name' => '全名',
            'password' => '密码',
            'auth_key' => 'Auth Key',
            'role' => '角色',
            'state' => '状态',
        ];
    }
	 /**
     * @return \yii\db\ActiveQuery
     */
    public function getChangeLogs()
    {
        return $this->hasMany(ChangeLog::className(), ['user_id' => 'id']);
    }
    public static function comboDataArray(){
        $models = self::find()->select('id,full_name')->all();
        $datas = [];
        foreach($models as $model){
            $data = [];
            $data['id'] = $model->id;
            $data['name'] = $model->full_name;
            $datas[] = $data;
        }
        return $datas;
    }
    public static function getLoginItems(){
        $models = self::find()->select('name,full_name')->all();
        return ArrayHelper::map($models, 'name', 'full_name');
    }

    public static function findWhereValid()
    {
        return self::find()->where(self::getValidWhere());
    }

    /**
     * @return array rightful where condition
     * @throws \Throwable
     */
    public static function getValidWhere()
    {
        $selfHigher = ['<', 'role', \Yii::$app->user->getIdentity()->role];
        $isSelf = ['=', 'id', \Yii::$app->user->id];
        return ['or', $isSelf, $selfHigher];
    }

    /**
	 * override
     */
    public static function getStateKeys() {
        return [self::STATE_NORMAL, self::STATE_LOCKED];
    }

    /**
     * User::$role bigger value, higher rank
     */
    const ROLE_OPERATOR = 0,
        ROLE_MANAGER = 3,
        ROLE_ADMIN = 9;

    /**
     * can comment out unrelated roles for the app
     * @param int $maxRole on of ROLE_ constants
     * @return array available role drop down items for this app
     */
    public static function getRoleItems($maxRole = null){
        $allRoles = [
            self::ROLE_OPERATOR => '操作员',
            self::ROLE_MANAGER => '管理员',
            self::ROLE_ADMIN => '技术支持',
        ];
        $roles = [];
        if($maxRole !== null) {
            foreach ($allRoles as $key => $role) {
                if ($key < $maxRole) {
                    $roles[$key] = $role;
                }
            }
        }else{
            $roles = $allRoles;
        }
        return $roles;
    }
    public function getRoleText(){
        return ArrayHelper::getValue(self::getRoleItems(), $this->role);
    }
    public static function whereForRole($role)
    {
        return self::find()->where(['<=', 'role', $role]);
    }
    const INIT_ID = 1;
    public static function makeSureInitialized() {
        if(self::findOne(self::INIT_ID) == null) {
            $user = new User();
            $user->id = self::INIT_ID;
            $user->name = 'Tech Support';
            $user->password = \Yii::$app->security->generatePasswordHash('Shifir1');
            $user->role = self::ROLE_ADMIN;
            $user->full_name = $user->getRoleText();
            $user->state = self::STATE_NORMAL;
            $tx = \Yii::$app->db->beginTransaction();
            try {
                $user->trySave();
                $tx->commit();
            } catch (\Exception $e) {
                $tx->rollBack();
                \Yii::error($e->getMessage() . "\n" . Json::encode($user), 'User.makeSureInitialized');
            }
        }
    }
    public function validatePassword($password) {
        return \Yii::$app->security->validatePassword($password, $this->password);
    }

    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            if($this->id != self::INIT_ID && !$this->isRightfulRole()){
                $message = '越权操作';
                \Yii::error( \Yii::$app->user->id . ' ' . $message . ' ' . Json::encode($this), 'User.beforeSave');
                throw new Exception($message);
            }
            //operator does not update login name
            if(!$insert && self::iAmOperator()){
                $this->name = $this->getOldAttribute('name');
            }
            if ($this->isNewRecord) {
                $this->auth_key = \Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return true;
    }

    public function getAuthKey() {
        return $this->auth_key;
    }

    public function getId() {
        return $this->id;
    }

    public function validateAuthKey($authKey) {
        return $authKey == $this->getAuthKey();
    }

    public static function findIdentity($id) {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(['access_token' => $token]);
    }

    public static function findByUsername($name) {
        return static::findOne(['name' => $name, 'state' => self::STATE_NORMAL]);
    }


    public function newUser()
    {
        $this->password = \Yii::$app->security->generatePasswordHash($this->password);
        if(empty($this->state)){
            $this->state = self::STATE_NORMAL;
        }
    }

    public function updateUser()
    {
        $meHigher = \Yii::$app->user->getIdentity()->role > $this->role;
        $selfNotRose = \Yii::$app->user->id == $this->id && \Yii::$app->user->getIdentity()->role >= $this->role;
        if(!($meHigher || $selfNotRose)){
            $message = '#' . \Yii::$app->user->id . ' 越权提拔';
            \Yii::error($message . ' ' . Json::encode($this), self::tableName() . '.updateUser');
            throw new \yii\base\Exception($message);
        }
        if(empty($this->password)) {
            //no change
            $this->password = $this->getOldAttribute('password');
        }else{
            //changed. hash it
            $this->password = \Yii::$app->security->generatePasswordHash($this->password);
        }
    }
    /**
     * operator (current user) has higher role than the user ($this), or is self
     * @return bool
     * @throws \Throwable
     */
    private function isRightfulRole()
    {
        $isSelf = \Yii::$app->user->id == $this->id;
        $selfHigher = \Yii::$app->user->getIdentity()->role > $this->role;
        return $selfHigher || ($isSelf && \Yii::$app->user->getIdentity()->role == $this->role);
    }

    public function setDeleted()
    {
        $this->state = self::STATE_DELETED;
        $this->update(false, ['state']);
    }

    public function hasCreateAccess(){
        return $this->role >= self::ROLE_MANAGER;
    }

    public function getNameLink(){
        return Html::a($this->name, ['user/view', 'id' => $this->id], ['title' => $this->full_name]);
    }

    public static function iAmOperator(){
        $user = Yii::$app->user->getIdentity();
        return $user && $user->role == self::ROLE_OPERATOR;
    }
    public static function iAmManager(){
        $user = Yii::$app->user->getIdentity();
        return $user && $user->role == self::ROLE_MANAGER;
    }
    public static function iAmAdmin(){
        $user = Yii::$app->user->getIdentity();
        return $user && $user->role == self::ROLE_ADMIN;
    }
}
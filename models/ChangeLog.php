<?php

namespace app\models;

use app\utils\Common;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "change_log".
 *
 * @property string $id
 * @property string $create_time 发生时间
 * @property string $user_id 操作员ID
 * @property string $user_full_name 操作员
 * @property string $table 表格
 * @property string $reference_id 相关记录ID
 * @property int $action 操作
 * @property string $old 备份
 *
 * @property User $user
 */
class ChangeLog extends \app\utils\CommonActiveRecord
{
	public static function tableLabel()
    {
        return '更新日志';
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'change_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'user_full_name', 'table', 'reference_id', 'action'], 'required'],
            [['user_id', 'reference_id', 'action'], 'integer'],
            [['old'], 'string'],
            [['user_full_name', 'table'], 'string', 'max' => 50],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => '发生时间',
            'user_id' => '操作员ID',
            'user_full_name' => '操作员',
            'table' => '表格',
            'reference_id' => '相关记录ID',
            'action' => '操作',
            'old' => '备份',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

	const ACTION_INSERTED = '新增',
        ACTION_UPDATED = '更新',
        ACTION_DELETED = '删除';

    /**
     * @param $action
     * @return int
     */
    public static function getActionId($action){
        return array_search($action, self::getAllActions());
    }
    public static function getActionTextFor($actionId){
        return ArrayHelper::getValue(self::getAllActions(), $actionId);
    }
    public static function getAllActions(){
        return [
            self::ACTION_INSERTED,
            self::ACTION_UPDATED,
            self::ACTION_DELETED,
        ];
    }
	
	public function getActionText() {
		return self::getActionTextFor($this->action);
	}

    public static function inserted($tableName, $id)
    {
        self::modified($tableName, $id, null, self::ACTION_INSERTED);
    }

    public static function updated($tableName, $id, $changedAttributes)
    {
        self::modified($tableName, $id, $changedAttributes, self::ACTION_UPDATED);
    }

    public static function deleted($tableName, $id, $attributes)
    {
        self::modified($tableName, $id, $attributes, self::ACTION_DELETED);
    }

    public static function modified($tableName, $refId, $attributes, $action){
        $log = new ChangeLog();
        $log->create_time = Common::now();
        $log->table = $tableName;
        $log->reference_id = $refId;
        $log->action = self::getActionId($action);
        if (!empty($attributes)) {
			$log->old = Json::encode($attributes);
		}
		if(!$log->save()){
            \Yii::error(self::tableName() . " 未保存 \n" . Json::encode($log) . "\n" . Json::encode($log->getErrorSummary(true)), self::className() . '.' . $action);
        }
    }
	/**
	 * referenced model with changed backup value ($old as attribute values)
	 * @return \app\utils\CommonActiveRecord
	 */
	public function getReferencedModel() {
		if($model = self::getModelFor($this->table)){
			$model->setAttributes(\yii\helpers\Json::decode($this->old));
		}
		return $model;
	}

	public static function getHistoryFor($table, $reference_id, &$modelNow) {
		if($modelNow = ChangeLog::getModelFor($table)){
			$modelNow = $modelNow::findOne($reference_id);
		}
		$class = ChangeLog::getClassFor($table);
		$changeLogs = ChangeLog::find()->where([
			'table' => $table,
			'reference_id' => $reference_id
		])->orderBy('create_time desc')->all();
		//latest state is the $modelNow
		$model = new $class;
		$model->setAttributes($modelNow->getAttributes());
		$history = [
			0 => ['model' => $model]
		];
		foreach ($changeLogs as $i => $changeLog){
			//operator and time
			$history[$i]['changeLog'] = $changeLog;
			//state
			if($i > 0){
				self::setBasedOnPreviousModel($history, $changeLogs, $i, $class);
			}
		}
		//set initial insert history
		$i = count($changeLogs);
		
		$history[$i]['changeLog'] = self::getChangeLogFromModel($modelNow);
		if(count($changeLogs) == 0){
			//return "无更新历史记录";
			//model was already set before loop
		}else{
			self::setBasedOnPreviousModel($history, $changeLogs, $i, $class);
		}
		return array_reverse($history);
	}

	private static function setBasedOnPreviousModel(&$history, $changeLogs, $i, $class) {
		$model = new $class;
		//based on previous state
		$model->setAttributes($history[$i - 1]['model']->getAttributes());
		//changeLog->old is the previous state, so shift
		$model->setAttributes(Json::decode($changeLogs[$i - 1]->old));
		$history[$i]['model'] = $model;
	}

	public static function getChangeLogFromModel($modelNow) {
		$changeLog = new ChangeLog();
		$changeLog->setAttributes($modelNow->getAttributes(['create_time', 'user_id', 'user_full_name']));
		$changeLog->create_time = $modelNow->create_time;
		$changeLog->action = self::getActionId(self::ACTION_INSERTED);
		return $changeLog;
	}

}